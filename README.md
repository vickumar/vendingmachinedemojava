### Vending Machine Demo Java

##### 0. Prerequisites

- [Java 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
- [Gradle 6](https://gradle.org/install/)
- [IntelliJ Community](https://www.jetbrains.com/idea/download)

##### 1. Create a New Spring Boot Web Application

- Goto [Spring Initializer](https://start.spring.io/).
- Choose `Gradle Project` for the type of project
- Choose `Java 11` under the `Project Metadata` section
- Add dependencies for: `Spring Web`, `Spring Data JPA`, and `Thymeleaf`
- Click `Generate` to create a new project
- Extract the downloaded `.zip` file to a location of your choice

![Spring Initializer](screenshots/spring-initializer.png)


##### 2. Import Your Project into IntelliJ

- Open 'IntelliJ' and choose to import a project.  
Select `Gradle` as the type.

![Select gradle](screenshots/intellij1.png)

- Select your project's location.  Use the gradle wrapper 
configuration and JDK 11.

![Select JVM version](screenshots/intellij2.png)


##### 3. Database Configuration

For this example, we will use an [in-memory database](https://www.h2database.com/html/main.html).
Different configurations can be applied for postgres, mysql, or oracle.

- Add under `dependencies` to `build.gradle` file:

```
dependencies {
    ...
    implementation 'com.h2database:h2:1.4.200'
    ... 
}     
    
```

- Add configuration settings under `src/main/resources/application.properties`:
```
spring.datasource.url=jdbc:h2:mem:vmachine
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=vmachine
spring.datasource.password=password
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

##### 4. Creating a Domain Model

- Add a new package `model.domain` and add classes for 
[Payment.java and Product.java](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/main/java/com/excella/vendingmachine/VendingMachineDemo/model/domain/)
- Create a `repositories` package and add 
a [PaymentRepository.java](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/main/java/com/excella/vendingmachine/VendingMachineDemo/repositories/)

##### 5. Dependency Injection: Adding Services
- Create a `service` package and add new services for 
[PaymentProcessor and VendingMachine](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/main/java/com/excella/vendingmachine/VendingMachineDemo/service/)
- Create an `impl` package and add [implementations](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/main/java/com/excella/vendingmachine/VendingMachineDemo/service/impl/)
for `CoinPaymentProcessor` and `VendingMachineImpl`
- Create [unit tests for your services](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/test/java/com/excella/vendingmachine/VendingMachineDemo/service/impl/)

##### 6. Adding a Controller
- Create a `controllers` package and add
a [VendingMachineController.java](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/main/java/com/excella/vendingmachine/VendingMachineDemo/controllers/VendingMacineController.java)

##### 7. Setting up a UI Template with Thymeleaf
- Create a package called `config`
- Add a `WebConfig.java` class under that package and add the contents:

```
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public ClassLoaderTemplateResolver templateResolver() {
        var templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("templates/");
        templateResolver.setCacheable(false);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML5");
        templateResolver.setCharacterEncoding("UTF-8");
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        var templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        return templateEngine;
    }

    @Bean
    public ViewResolver viewResolver() {
        var viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setCharacterEncoding("UTF-8");
        return viewResolver;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/vendingMachine").setViewName("vendingMachine");
    }
}
```

- Under `src/main/resources/templates`, add a new file, `vendingMachine.html`:
```
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Vending Machine Demo</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <h3>Vending Machine</h3>
    <form action="#" th:action="@{/vendingMachine}" th:object="${vendingMachineState}" method="post">
        <p><input id="insertCoin" type="submit" value="Insert Coin" name="insert" /></p>
        <p><input id="releaseChange" type="submit" value="Release Change" name="release" /></p>

        <div>
            <p id="totalAmount">
            Balance: <input disabled id="balanceAmount" th:field="*{balance}"
                            style="width: 100px" /> cents
            </p>
        </div>
        <div>
            <p>
            Released Change: <input disabled id="releasedChangeAmount" th:field="*{releasedChange}"
                                    style="width: 100px" /> cents
            </p>
        </div>
    </form>
</body>
</html>
```

##### 8. Running the application
- In a terminal, type: `gradle bootrun`
- Open a web browser, and navigate to: `http://localhost:8080/vendingMachine`
- To run unit tests, type: `gradle test`
- To fully build the application, type: `gradle clean build`

![Running application](screenshots/application.png)

##### 9. Integration Tests

- Update `build.gradle` to include an `inttest` task for 
running integration tests

```
...
// Add sourceSets and configurations
sourceSets {
	inttest {
		java
		resources
		compileClasspath +=  main.output + test.output
		runtimeClasspath += main.output + test.output
	}
}

configurations {
	inttestCompile.extendsFrom testCompile
	inttestRuntime.extendsFrom testRuntime
	inttestImplementation.extendsFrom testImplementation

}
...
dependencies {
...
    inttestImplementation('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
} 
 

task inttest(type: Test) {
	testClassesDirs = sourceSets.inttest.output.classesDirs
	classpath = sourceSets.inttest.runtimeClasspath
	useJUnitPlatform()
} 
```

- Create [integration tests](https://bitbucket.org/vickumar/vendingmachinedemojava/src/master/src/inttest/java/com/excella/vendingmachine/VendingMachineDemo/) for the controller and service under 
a `src/inttest/java` directory that you create.

- Run you integration tests task: `gradle inttest`

 


