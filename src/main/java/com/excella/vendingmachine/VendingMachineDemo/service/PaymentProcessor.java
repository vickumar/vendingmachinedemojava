package com.excella.vendingmachine.VendingMachineDemo.service;

public interface PaymentProcessor {
    int getPayment();
    boolean isPaymentMade();
    void processPayment(int amount);
    void processPurchase();
    void clearPayments();
}
