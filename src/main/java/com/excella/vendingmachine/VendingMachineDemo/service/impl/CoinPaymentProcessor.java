package com.excella.vendingmachine.VendingMachineDemo.service.impl;

import com.excella.vendingmachine.VendingMachineDemo.service.PaymentProcessor;
import com.excella.vendingmachine.VendingMachineDemo.dao.payments.PaymentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoinPaymentProcessor implements PaymentProcessor {
    private PaymentDao paymentDao;

    @Autowired
    public CoinPaymentProcessor(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }


    @Override
    public int getPayment() {
        return paymentDao.retrieve();
    }

    @Override
    public boolean isPaymentMade() {
        return getPayment() >= 50;
    }

    @Override
    public void processPayment(int amount) {
        paymentDao.savePayment(amount);
    }

    @Override
    public void processPurchase() {
        paymentDao.savePurchase();
    }

    @Override
    public void clearPayments() {
        paymentDao.clearPayments();
    }
}
