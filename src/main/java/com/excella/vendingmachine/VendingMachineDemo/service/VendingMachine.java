package com.excella.vendingmachine.VendingMachineDemo.service;

import com.excella.vendingmachine.VendingMachineDemo.model.domain.Product;

public interface VendingMachine {
    double getBalance();
    String getMessage();
    Product buyProduct();
    void insertCoin();
    int releaseChange();
}
