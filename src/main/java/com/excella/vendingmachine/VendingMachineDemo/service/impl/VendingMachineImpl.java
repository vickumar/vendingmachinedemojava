package com.excella.vendingmachine.VendingMachineDemo.service.impl;

import com.excella.vendingmachine.VendingMachineDemo.model.domain.Product;
import com.excella.vendingmachine.VendingMachineDemo.service.PaymentProcessor;
import com.excella.vendingmachine.VendingMachineDemo.service.VendingMachine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VendingMachineImpl implements VendingMachine {
    private PaymentProcessor paymentProcessor;
    private String message;

    @Autowired
    public VendingMachineImpl(PaymentProcessor paymentProcessor) {
        this.paymentProcessor = paymentProcessor;
        this.message = "";
    }

    @Override
    public double getBalance() {
        return paymentProcessor.getPayment();
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Product buyProduct() {
        if (paymentProcessor.isPaymentMade()) {
            paymentProcessor.processPurchase();
            message = "You bought a product";
            return new Product();
        }
        message = "Please insert money";
        return null;
    }

    @Override
    public void insertCoin() {
        paymentProcessor.processPayment(25);
    }

    @Override
    public int releaseChange() {
        var payment = paymentProcessor.getPayment();
        if (payment > 0) {
            paymentProcessor.clearPayments();
        }
        return payment;
    }
}
