package com.excella.vendingmachine.VendingMachineDemo.repositories;

import com.excella.vendingmachine.VendingMachineDemo.model.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {}
