package com.excella.vendingmachine.VendingMachineDemo.dao.payments;

public interface PaymentDao {
    int retrieve();
    void savePayment(int amount);
    void savePurchase();
    void clearPayments();
}
