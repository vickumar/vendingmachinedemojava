package com.excella.vendingmachine.VendingMachineDemo.dao.payments.impl;

import com.excella.vendingmachine.VendingMachineDemo.model.domain.Payment;
import com.excella.vendingmachine.VendingMachineDemo.repositories.PaymentRepository;
import com.excella.vendingmachine.VendingMachineDemo.dao.payments.PaymentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class DbPaymentDao implements PaymentDao {
    private final int purchasePrice = 50;
    private PaymentRepository paymentRepository;

    @Autowired
    public DbPaymentDao(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }


    @Override
    public int retrieve() {
        return paymentRepository.findById(1L).map(Payment::getValue).orElse(0);
    }

    @Override
    public void savePayment(int amount) {
        var payment = paymentRepository.findById(1L);
        payment.ifPresentOrElse(
                p -> {
                    p.setValue(p.getValue() + amount);
                    paymentRepository.save(p);
                    },
                () -> {
                    var newPayment = new Payment();
                    newPayment.setId(1L);
                    newPayment.setValue(amount);
                    paymentRepository.save(newPayment);
                }
        );
    }

    @Override
    public void savePurchase() {
        var payment = paymentRepository.findById(1L);
        payment.ifPresent(
                p -> {
                    p.setValue(p.getValue() - purchasePrice);
                    paymentRepository.save(p);
                }
        );
    }

    @Override
    public void clearPayments() {
        var payment = paymentRepository.findById(1L);
        payment.ifPresent(
                p -> {
                    p.setValue(0);
                    paymentRepository.save(p);
                }
        );
    }
}
