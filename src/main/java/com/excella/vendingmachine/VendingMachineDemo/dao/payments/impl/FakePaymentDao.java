package com.excella.vendingmachine.VendingMachineDemo.dao.payments.impl;

import com.excella.vendingmachine.VendingMachineDemo.dao.payments.PaymentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FakePaymentDao implements PaymentDao {
    private final int purchasePrice = 50;
    private int balance;

    @Autowired
    public FakePaymentDao() { }


    @Override
    public int retrieve() {
        return balance;
    }

    @Override
    public void savePayment(int amount) {
        balance += amount;
    }

    @Override
    public void savePurchase() {
        balance -= purchasePrice;
    }

    @Override
    public void clearPayments() {
        balance = 0;
    }
}
