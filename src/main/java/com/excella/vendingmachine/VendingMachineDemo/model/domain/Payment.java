package com.excella.vendingmachine.VendingMachineDemo.model.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "payment")
public class Payment {
    @Id
    private Long id;

    @Column(name = "value", nullable = false)
    private Integer value;


    public Payment() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equals(id, payment.id) &&
                Objects.equals(value, payment.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value);
    }
}
