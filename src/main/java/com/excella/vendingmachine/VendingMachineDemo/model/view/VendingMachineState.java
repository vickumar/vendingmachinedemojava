package com.excella.vendingmachine.VendingMachineDemo.model.view;

public class VendingMachineState {
    private double balance;
    private int releasedChange;

    public VendingMachineState() {
        this.balance = 0d;
        this.releasedChange = 0;
    }

    public VendingMachineState(double balance, int releasedChange) {
        this.balance = balance;
        this.releasedChange = releasedChange;
    }

    public double getBalance() { return this.balance; }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getReleasedChange() { return this.releasedChange; }

    public void setReleasedChange(int releasedChange) {
        this.releasedChange = releasedChange;
    }
}
