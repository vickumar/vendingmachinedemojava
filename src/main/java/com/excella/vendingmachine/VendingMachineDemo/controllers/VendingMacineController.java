package com.excella.vendingmachine.VendingMachineDemo.controllers;

import com.excella.vendingmachine.VendingMachineDemo.model.view.VendingMachineState;
import com.excella.vendingmachine.VendingMachineDemo.service.VendingMachine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class VendingMacineController {
    private VendingMachine vendingMachine;
    Logger logger = LoggerFactory.getLogger(VendingMacineController.class);

    @Autowired
    public VendingMacineController(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }

    @RequestMapping(value = "/vendingMachine", method= RequestMethod.GET)
    public void showVendingMachine(Model model) {
        var vendingMachineState = new VendingMachineState();
        model.addAttribute("vendingMachineState", vendingMachineState);
    }


    @RequestMapping(value = "/vendingMachine", method= RequestMethod.POST, params = "release")
    public void releaseChange(@ModelAttribute(value="vendingMachineState")
                                          VendingMachineState vendingMachineState) {
        logger.info("Releasing change from vending machine");
        vendingMachineState.setReleasedChange(vendingMachine.releaseChange());
        vendingMachineState.setBalance(vendingMachine.getBalance());

    }

    @RequestMapping(value = "/vendingMachine", method= RequestMethod.POST, params = "insert")
    public void insertCoin(@ModelAttribute(value="vendingMachineState")
                                       VendingMachineState vendingMachineState) {
        logger.info("Inserting coin in vending machine");
        vendingMachine.insertCoin();
        vendingMachineState.setBalance(vendingMachine.getBalance());
    }
}
