package com.excella.vendingmachine.VendingMachineDemo.controllers;

import com.excella.vendingmachine.VendingMachineDemo.model.view.VendingMachineState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.ExtendedModelMap;


@SpringBootTest
public class VendingMachineControllerITest {
    @Autowired
    private VendingMacineController vendingMacineController;

    @Test
    void testWhenIndexIsLoadedBalanceIsZero() {
        var vendingMachineState = new ExtendedModelMap();
        vendingMacineController.showVendingMachine(vendingMachineState);
        var model = (VendingMachineState) vendingMachineState.getAttribute("vendingMachineState");
        Assertions.assertEquals(model.getBalance(), 0);
    }

    @Test
    void testHasBalanceOf25AfterSingleInsert() {
        var vendingMachineState = new VendingMachineState();
        vendingMacineController.insertCoin(vendingMachineState);
        Assertions.assertEquals(vendingMachineState.getBalance(), 25);
    }

    @Test
    void testReleasingChangeWithMoneyReturnsValue() {
        var vendingMachineState = new VendingMachineState();
        vendingMacineController.insertCoin(vendingMachineState);
        vendingMacineController.releaseChange(vendingMachineState);
        Assertions.assertEquals(vendingMachineState.getReleasedChange(), 25);
    }

    @Test
    void testReleasingChangeAfterSingleInsertReturnsZeroBalance() {
        var vendingMachineState = new VendingMachineState();
        vendingMacineController.insertCoin(vendingMachineState);
        vendingMacineController.releaseChange(vendingMachineState);
        Assertions.assertEquals(vendingMachineState.getBalance(), 0);
    }
}
