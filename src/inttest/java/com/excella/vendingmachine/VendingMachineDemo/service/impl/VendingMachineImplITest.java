package com.excella.vendingmachine.VendingMachineDemo.service.impl;

import com.excella.vendingmachine.VendingMachineDemo.service.PaymentProcessor;
import com.excella.vendingmachine.VendingMachineDemo.service.VendingMachine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class VendingMachineImplITest {
    private VendingMachine vendingMachine;

    @Autowired
    private PaymentProcessor paymentProcessor;

    @BeforeEach
    private void beforeEach() {
        paymentProcessor.clearPayments();
        vendingMachine = new VendingMachineImpl(paymentProcessor);
    }

    @Test
    void testBalanceIncreasesBy25WhenOneCoinIsInserted() {
        vendingMachine.insertCoin();
        Assertions.assertEquals(vendingMachine.getBalance(), 25);
    }

    @Test
    void testReleasingChangeWithNoMoneyIsZero() {
        var changeReleased = vendingMachine.releaseChange();
        Assertions.assertEquals(changeReleased, 0);
    }

    @Test
    void testReleasingChangeWithOneCoinInsertedIs25() {
        vendingMachine.insertCoin();
        var changeReleased = vendingMachine.releaseChange();
        Assertions.assertEquals(changeReleased, 25);
    }

    @Test
    void testReleasingChangeWith3CoinsAndBuyingProductIs25() {
        vendingMachine.insertCoin();
        vendingMachine.insertCoin();
        vendingMachine.insertCoin();
        vendingMachine.buyProduct();
        var changeReleased = vendingMachine.releaseChange();
        Assertions.assertEquals(changeReleased, 25);
    }
}
