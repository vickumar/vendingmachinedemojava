package com.excella.vendingmachine.VendingMachineDemo.service.impl;

import com.excella.vendingmachine.VendingMachineDemo.service.PaymentProcessor;
import com.excella.vendingmachine.VendingMachineDemo.service.VendingMachine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class VendingMachineImplTests {
    private PaymentProcessor paymentProcessor;
    private VendingMachine vendingMachine;

    @BeforeEach
    private void beforeEach() {
        this.paymentProcessor = mock(PaymentProcessor.class);
        this.vendingMachine = new VendingMachineImpl(paymentProcessor);
    }

    @Test
    void testReleasedChangeIsZeroWhenNoMoneyIsInserted() {
        when(paymentProcessor.getPayment()).thenReturn(0);
        var change = vendingMachine.releaseChange();
        assertEquals(change, 0);
    }

    @Test
    void testReleasedChangeWhenOneCoinIsInserted() {
        when(paymentProcessor.getPayment()).thenReturn(25);
        var change = vendingMachine.releaseChange();
        assertEquals(change, 25);
    }

    @Test
    void testBuyProductWithNoPaymentReturnsNull() {
        when(paymentProcessor.isPaymentMade()).thenReturn(false);
        var product = vendingMachine.buyProduct();
        assertNull(product);
    }

    @Test
    void testBuyProductWithMoneyInsertedReturnsProduct() {
        when(paymentProcessor.isPaymentMade()).thenReturn(true);
        var product = vendingMachine.buyProduct();
        assertNotNull(product);
    }

    @Test
    void testBuyProductWithPaymentCallsProcessPurchase() {
        when(paymentProcessor.isPaymentMade()).thenReturn(true);
        vendingMachine.buyProduct();
        verify(paymentProcessor, times(1)).processPurchase();
    }

    @Test
    void testBuyProductWithNoPaymentNeverCallsProcessPurchase() {
        when(paymentProcessor.isPaymentMade()).thenReturn(false);
        vendingMachine.buyProduct();
        verify(paymentProcessor, never()).processPurchase();
    }

    @Test
    void testMessageWhenNoMoneyIsInserted() {
        when(paymentProcessor.isPaymentMade()).thenReturn(false);
        vendingMachine.buyProduct();
        assertEquals(vendingMachine.getMessage(), "Please insert money");
    }

    @Test
    void testMessagePromptWhenMoneyIsInserted() {
        when(paymentProcessor.isPaymentMade()).thenReturn(true);
        vendingMachine.buyProduct();
        assertEquals(vendingMachine.getMessage(), "You bought a product");
    }

    @Test
    void testReleaseChangeWhenCoinInsertedCallsResetPayment() {
        when(paymentProcessor.getPayment()).thenReturn(25);
        vendingMachine.releaseChange();
        verify(paymentProcessor, times(1)).clearPayments();
    }

    @Test
    void testReleasedChangeWhenNoCoinsNeverCallsResetPayment() {
        vendingMachine.releaseChange();
        verify(paymentProcessor, never()).clearPayments();

    }
}
