package com.excella.vendingmachine.VendingMachineDemo.service.impl;

import com.excella.vendingmachine.VendingMachineDemo.service.PaymentProcessor;
import com.excella.vendingmachine.VendingMachineDemo.dao.payments.PaymentDao;
import com.excella.vendingmachine.VendingMachineDemo.dao.payments.impl.FakePaymentDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class CoinPaymentProcessorTests {
    private PaymentDao paymentDao;
    private PaymentProcessor paymentProcessor;

    @BeforeEach
    private void beforeEach() {
        this.paymentDao = mock(FakePaymentDao.class);
        this.paymentProcessor = new CoinPaymentProcessor(paymentDao);
    }

    @Test
    void testBalanceIsZeroWhenPaymentHasNoMoney() {
        when(paymentDao.retrieve()).thenReturn(0);
        var balance = paymentProcessor.getPayment();
        assertEquals(balance, 0);
    }

    @Test
    void testBalanceIsNotZeroWhenPaymentHasMoney() {
        when(paymentDao.retrieve()).thenReturn(25);
        var balance = paymentProcessor.getPayment();
        assertEquals(balance, 25);
    }

    @Test
    void testPaymentIsNotMadeWhenPaymentHasMoney() {
        when(paymentDao.retrieve()).thenReturn(0);
        assertFalse(paymentProcessor.isPaymentMade());
    }

    @Test
    void testPaymentIsMadeWhenPaymentHas50Cents() {
        when(paymentDao.retrieve()).thenReturn(50);
        assertTrue(paymentProcessor.isPaymentMade());
    }

    @Test
    void testPaymentIsMadeWhenPaymentHasMoreThan50Cents() {
        when(paymentDao.retrieve()).thenReturn(75);
        assertTrue(paymentProcessor.isPaymentMade());
    }

    @Test
    void testProcessPaymentCallsSavePayment() {
        paymentProcessor.processPayment(25);
        verify(paymentDao, times(1)).savePayment(25);
    }

    @Test
    void testProcessPurchaseCallsSavePurchase() {
        paymentProcessor.processPurchase();
        verify(paymentDao, times(1)).savePurchase();
    }

    @Test
    void testClearPaymentsCallsClearPaymentsService() {
        when(paymentDao.retrieve()).thenReturn(25);
        paymentProcessor.clearPayments();
        verify(paymentDao, times(1)).clearPayments();
    }
}
